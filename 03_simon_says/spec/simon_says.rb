def echo msj
	msj
end

def shout msj
	msj.upcase
end

def repeat msj,repeatFrequency=2
	([msj]*repeatFrequency).join(" ")
end

def start_of_word word,limit
	word[0,limit]
end

def first_word msj
	arrayLetters = msj.split()
	arrayLetters[0]
end

def titleize msj
	inputStringToArray = msj.split
	arrayWordsCapitalized = []
	wordToNotCapitalize = ["and","over","the"]

	for key in  0...inputStringToArray.length
		for keyNotCapitalize in 0...wordToNotCapitalize.length

			if wordToNotCapitalize[keyNotCapitalize] != inputStringToArray[key]
				arrayWordsCapitalized.push(inputStringToArray[key].capitalize+" ")
				break;
			else
				arrayWordsCapitalized.push(inputStringToArray[key]+" ")
				break;
			end
		end
	end

	arrayWordsCapitalized.join.strip
end
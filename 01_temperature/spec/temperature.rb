def ftoc (farenheitGrades)
	return (((farenheitGrades-32)*5)/9)
end

def ctof (celciusGrades)
	return (((celciusGrades*9.0)/5.0)+32.0)
end
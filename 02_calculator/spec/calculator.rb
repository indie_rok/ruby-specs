def add (num1,num2)
	return num1+num2
end

def subtract(num1,num2)
	return num1-num2
end

def sum(array=[])
	if !array.any?
		return 0
	else
		temp=0;
		array.each do|num|
			temp= num+temp
		end
		return temp
	end
end

def multiply(array=[])
	if !array.any?
		return 0
	else
		array.inject(:*)
 	end
end

def power(num,power)
	num**power
end

def factorial(num)
	(1..num).inject(:*) || 1
end
def translate msj
	msjArray = msj.split("")

	if self.is_vowel(msj[0]) == true
		msj+"ay"
	else
		consonantIndex = self.check_how_many_consonants_after_second_letter(msjArray)
		newWord =  msj[consonantIndex,msjArray.length] + msj[0,consonantIndex]+"ay"
		
	end
end

public
def is_vowel letter
	vowels = ["a","e","i","o","u"]
	for k in 0..vowels.length
		if letter == vowels[k]
			return true
		end
	end
end

def check_how_many_consonants_after_second_letter word
	counter=1;
	for key in 1..word.length
		if self.is_vowel(word[key]) != true
			counter += 1
		else
			break
		end
	end

	counter
end